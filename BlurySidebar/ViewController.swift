//
//  ViewController.swift
//  BlurySidebar
//
//  Created by Anders Zetterström on 2015-04-09.
//  Copyright (c) 2015 Anders Zetterström. All rights reserved.
//

import UIKit


class ViewController: UIViewController/*, SideBarDelegate */{
    // bakgrundsbild
    @IBOutlet weak var imageView: UIImageView!
    // för att kunna placera items
    @IBOutlet weak var placing11: UIView!
    @IBOutlet weak var placing12: UIView!
    @IBOutlet weak var placing13: UIView!
    
    @IBOutlet weak var placing21: UIView!
    @IBOutlet weak var placing22: UIView!
    @IBOutlet weak var placing23: UIView!
    
    @IBOutlet weak var placing31: UIView!
    @IBOutlet weak var placing32: UIView!
    @IBOutlet weak var placing33: UIView!
    
    @IBOutlet weak var placing41: UIView!
    @IBOutlet weak var placing42: UIView!
    @IBOutlet weak var placing43: UIView!
    
    // instans av SideBar
    var sideBar: SideBar = SideBar()
    var location = CGPoint(x: 0, y: 0)
    var placingList =  [UIView]()
    var itemList = [Item]()
    
    var itemViewList = [UIView]()
    
    var animator: UIDynamicAnimator?
    var snapPoint = CGPoint(x: 0, y: 0)
   
    

    
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        
      let touch = touches.first as? UITouch
        let location = touch!.locationInView(self.view)
      
        for sceneView in itemViewList {
            
            
            if sceneView.frame.contains(location) {
                
                selectedItemView = sceneView
                applyPlainShadow(selectedItemView!)
                break
            }
        }
    
    }
    
    
    
    override func touchesMoved(touches: Set<NSObject>, withEvent event: UIEvent) {
        // man klickar var som helst på skärmen
       var touch : UITouch! = touches.first as? UITouch
      //  var touch : UITouch! = touches.anyObject() as? UITouch
        // vart man tryckte i vyn
        location = touch.locationInView(self.view)
        selectedItemView?.center = location
        
    }
    
    override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
      //  var touch : UITouch! = touches.anyObject() as UITouch
      //  location = touch.locationInView(self.view)
        super.touchesEnded(touches, withEvent: event)
        
        if let activeItemView = selectedItemView {
            placingView(activeItemView)
            removePlainShadow(activeItemView)
        } else {
            NSLog("else activeItemView = selectedItemView")
        }
        
    //    placingView(selectedItemView!)
    //    removePlainShadow(selectedItemView!)
        
    }
    
    func runAnimation() {
        selectedItemView?.alpha = 0.0
        
        UIView.animateWithDuration(1.5, animations: {
           selectedItemView?.alpha = 1.0
            return
        })
        
    }
    
    func rowOne(selectedView: UIView) {
        if selectedView.frame.intersects(placing11.frame) {
            snapPoint = placing11.center
            
        }
        
        if selectedView.frame.intersects(placing12.frame) {
            snapPoint = placing12.center
        }
        
        if selectedView.frame.intersects(placing13.frame) {
            snapPoint = placing13.center
        }
    }
    
    
    
    func rowTwo(selectedView: UIView) {
        if selectedView.frame.intersects(placing21.frame) {
            snapPoint = placing21.center
        }
        
        if selectedView.frame.intersects(placing22.frame) {
            snapPoint = placing22.center
        }
        
        if selectedView.frame.intersects(placing23.frame) {
            snapPoint = placing23.center
        }
    }
    
    func rowThree(selectedView: UIView) {
        if selectedView.frame.intersects(placing31.frame) {
            snapPoint = placing31.center
        }
        
        if selectedView.frame.intersects(placing32.frame) {
            snapPoint = placing32.center
        }
        
        if selectedView.frame.intersects(placing33.frame) {
            snapPoint = placing33.center
        }
    }
    
    func rowFour(selectedView: UIView) {
        if selectedView.frame.intersects(placing41.frame) {
            snapPoint = placing41.center
        }
        
        if selectedView.frame.intersects(placing42.frame) {
            snapPoint = placing42.center
        }
        
        if selectedView.frame.intersects(placing43.frame) {
            snapPoint = placing43.center
        }
    }
    

    func placingView(selectedView: UIView){
        self.animator = UIDynamicAnimator(referenceView: self.view)
        
        rowOne(selectedView)
        rowTwo(selectedView)
        rowThree(selectedView)
        rowFour(selectedView)
        
        var snap = UISnapBehavior(item: selectedView, snapToPoint: snapPoint)
        self.animator!.addBehavior(snap)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        var item1 = Item(groupe: "vehicle", eventText: "electric car", isActive : false)
        var item2 = Item(groupe: "vehicle", eventText: "boat", isActive : false)
        var item3 = Item(groupe: "vehicle", eventText: "bus", isActive : false)
        var item4 = Item(groupe: "vehicle", eventText: "excavator", isActive : false)
        var item5 = Item(groupe: "vehicle", eventText: "train", isActive : false)
        var item6 = Item(groupe: "vehicle", eventText: "subway", isActive : false)
        var item7 = Item(groupe: "vehicle", eventText: "tram", isActive : false)
        var item8 = Item(groupe: "vehicle", eventText: "truck", isActive : false)
        var item9 = Item(groupe: "vehicle", eventText: "airplane", isActive : false)
        
        itemList.append(item1)
        itemList.append(item2)
        itemList.append(item3)
        itemList.append(item4)
        itemList.append(item5)
        itemList.append(item6)
        itemList.append(item7)
        itemList.append(item8)
        itemList.append(item9)
        
        NSLog("list-count: \(itemList.count)")
    
        placingList.append(placing11)
        placingList.append(placing12)
        placingList.append(placing13)
        placingList.append(placing21)
        placingList.append(placing22)
        placingList.append(placing23)
        placingList.append(placing31)
        placingList.append(placing32)
        placingList.append(placing33)
        placingList.append(placing41)
        placingList.append(placing42)
        placingList.append(placing43)
        
        NSLog("placingList : \(placingList.count)")
        
        sideBar = SideBar(sourceView: self.view, originViewController:self, itemList: itemList)
  
    }
    
    
    
    override func viewDidAppear(animated: Bool) {
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /*
    func sideBarDidSelectButtonAtIndex(index: Int) {
    if index == 0 {
    imageView.backgroundColor = UIColor.blueColor()
    imageView.image = nil
    NSLog("index if")
    } else if index == 1 {
    imageView.backgroundColor = UIColor.greenColor()
    NSLog("index else")
    }
    }*/
    
    
    @objc func createItem(indexPath: Int ){
        
        NSLog("running createItem()")
        
        let image = UIImage(named: itemList[indexPath].eventText)!
        
        selectedItemView = UIView(frame: CGRect(x: 400, y: 40, width: 150, height: 150))
        
        if let theItemView = selectedItemView {
            theItemView.backgroundColor = UIColor.whiteColor()
            var imageView : UIImageView = UIImageView(image: image)
            theItemView.addSubview(imageView)
            //placering och  storlek på imageViewn
            imageView.frame = CGRectMake(10, 20, 130, 90)
            /// sätter den i mitten av viewn
            imageView.clipsToBounds = true
            
        }
        
        
        // textlabel pos i itemview
        var labelX : CGFloat = 50.0
        var labelY : CGFloat = 50.0
        
        var groupeLabel = UILabel(frame: CGRectMake(0, 0, 150, 20))
        //  groupeLabel.center = CGPoint(x: labelX, y: 15)
        groupeLabel.textAlignment = NSTextAlignment.Center
        
        var eventTextLabel = UILabel(frame: CGRectMake(0, 115, 150, 30))
        // eventTextLabel.center = CGPoint(x: labelX, y: labelY+40)
        eventTextLabel.textAlignment = NSTextAlignment.Center
        
        // sätter grupptexten
        groupeLabel.text = itemList[indexPath].groupe
        // sätter eventtexten
        eventTextLabel.text = itemList[indexPath].eventText
        eventTextLabel.font = UIFont(name: eventTextLabel.font.fontName, size: 25)
        
        // lägger till alla views
        selectedItemView?.addSubview(groupeLabel)
        selectedItemView?.addSubview(eventTextLabel)
        
        itemViewList.append(selectedItemView!)
        self.view.addSubview(selectedItemView!)
        runAnimation()
        
    }
    
    func applyPlainShadow(view: UIView) {
        var layer = view.layer
        
        layer.shadowColor = UIColor.blackColor().CGColor
        layer.shadowOffset = CGSize(width: 0, height: 10)
        layer.shadowOpacity = 0.4
        layer.shadowRadius = 5
    }
    
    func removePlainShadow(view: UIView) {
       var layer = view.layer
        
        layer.shadowColor = nil
        layer.shadowOffset = CGSize(width: 0, height: 0)
        layer.shadowOpacity = 0.0
        layer.shadowRadius = 0
        
    }
    
    
}




























//
//  SideBar.swift
//  BlurySidebar
//
//  Created by Anders Zetterström on 2015-04-09.
//  Copyright (c) 2015 Anders Zetterström. All rights reserved.
//

import UIKit

@objc protocol SideBarDelegate{
    func sideBarDidSelectButtonAtIndex(index:Int)
  //  optional func sideBarWillClose()
    optional func sideBarWillOpen()
}

class SideBar: NSObject, SideBarTableViewControllerDelegate {
    // bredden på sidebar
    let barWidth:CGFloat = 190.0
    
    // avstånd från överkant
    let sideBarTableViewTopInset:CGFloat = 0.0
    
    let sideBarContainerView:UIView = UIView()
    let originView: UIView!
    let originViewController : ViewController!
    let sideBarTableViewController:SideBarTableViewController = SideBarTableViewController()
    
    var animator:  UIDynamicAnimator!
    var delegate: SideBarDelegate?
    var isSideBarOpen: Bool = false
   
    override init(){
        super.init()
    }
    
     init(sourceView:UIView, originViewController : ViewController, itemList: Array<Item>) {
        
        originView = sourceView
        self.originViewController = originViewController
        
        sideBarTableViewController.tableData = itemList
        
        sideBarTableViewController.sourceView = sourceView
        super.init()
        setupSideBar()
        
        animator = UIDynamicAnimator(referenceView: originView)
        
        showSideBar(true)
        delegate?.sideBarWillOpen?()
        
        /*
        let showGestureRecognizer: UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: "handleSwipe:")
        showGestureRecognizer.direction = UISwipeGestureRecognizerDirection.Right
        originView.addGestureRecognizer(showGestureRecognizer)
        
        let hideGestureRecognizer: UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: "handleSwipe:")
        hideGestureRecognizer.direction = UISwipeGestureRecognizerDirection.Left
        originView.addGestureRecognizer(hideGestureRecognizer)
        */
    }
    
    func setupSideBar(){
        sideBarContainerView.frame = CGRectMake(-barWidth - 1, originView.frame.origin.y, barWidth, originView.frame.size.height)
        
        sideBarContainerView.backgroundColor = UIColor.clearColor()
        
        sideBarContainerView.clipsToBounds = false
        
        originView.addSubview(sideBarContainerView)
        
        // sätter blur på sidebaren
        let blurEffect = UIBlurEffect(style: .Dark)
        let blurView = UIVisualEffectView(effect: blurEffect)
        blurView.frame = sideBarContainerView.bounds
        sideBarContainerView.addSubview(blurView)
        
        
        sideBarTableViewController.delegate = self
        // sätter tableviewn till samma storlek som sidebaren
        sideBarTableViewController.tableView.frame = sideBarContainerView.bounds
        sideBarTableViewController.tableView.clipsToBounds = false
        
        sideBarTableViewController.tableView.scrollsToTop = false
        sideBarTableViewController.tableView.contentInset = UIEdgeInsetsMake(sideBarTableViewTopInset, 0, 0, 0)
        
        sideBarTableViewController.tableView.reloadData()
        sideBarContainerView.addSubview(sideBarTableViewController.tableView)
        
    }
    /*
    func handleSwipe(recognizer: UISwipeGestureRecognizer){
        if recognizer.direction == UISwipeGestureRecognizerDirection.Left{
            showSideBar(false)
         //   delegate?.sideBarWillClose?()
        }else {
            showSideBar(true)
          //  delegate?.sideBarWillOpen?()
        }
    }*/
    
    func showSideBar(shouldOpen: Bool) {
        animator.removeAllBehaviors()
        isSideBarOpen == shouldOpen
        
        let gravityX: CGFloat = (shouldOpen) ? 0.5 : -0.5
        let magnitude: CGFloat = (shouldOpen) ? 20 : -20
        let boundaryX: CGFloat = (shouldOpen) ? barWidth: -barWidth - 1
        
        let gravityBehavior: UIGravityBehavior = UIGravityBehavior(items: [sideBarContainerView])
        gravityBehavior.gravityDirection = CGVectorMake(gravityX, 0)
        animator.addBehavior(gravityBehavior)
        
        let collisionBehavior: UICollisionBehavior = UICollisionBehavior(items: [sideBarContainerView])
        collisionBehavior.addBoundaryWithIdentifier("sideBarBoundary", fromPoint: CGPointMake(boundaryX, 20), toPoint: CGPointMake(boundaryX, originView.frame.size.height))
        animator.addBehavior(collisionBehavior)
        
        let pushBehavior: UIPushBehavior = UIPushBehavior(items: [sideBarContainerView], mode: UIPushBehaviorMode.Instantaneous)
        pushBehavior.magnitude = magnitude
        animator.addBehavior(pushBehavior)
        
        let sideBarBehavior: UIDynamicItemBehavior = UIDynamicItemBehavior(items: [sideBarContainerView])
        sideBarBehavior.elasticity = 0.3
        animator.addBehavior(sideBarBehavior)
        
        
    }
    
    func sideBarControlDidSelectRow(indexPath: NSIndexPath) {
        var index = indexPath.row
        originViewController.createItem(index)
        delegate?.sideBarDidSelectButtonAtIndex(indexPath.row)
        
        NSLog("sidebarControllDidSelectRow")
    }
        
    
}

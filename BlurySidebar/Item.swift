//
//  Item.swift
//  BlurySidebar
//
//  Created by Anders Zetterström on 2015-04-09.
//  Copyright (c) 2015 Anders Zetterström. All rights reserved.
//

import UIKit


class Item: NSObject {
    
    
    var groupe : String
    var eventText : String
    var isActive : Bool

    init (groupe: String, eventText: String, isActive : Bool){
        self.groupe =  groupe
        self.eventText = eventText
        self.isActive = isActive
    }
}

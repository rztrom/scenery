//
//  ItemCellNibTableViewCell.swift
//  BlurySidebar
//
//  Created by Anders Zetterström on 2015-04-26.
//  Copyright (c) 2015 Anders Zetterström. All rights reserved.
//

import UIKit

class ItemCellNibTableViewCell: UITableViewCell {
    
    let itemView = UIView()
    let groupLabel = UILabel()
    let eventLabel = UILabel()
    let cellImageView = UIImageView()
    
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initViews()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initViews()
    }
    
    func initViews() {
        
        itemView.frame = CGRect(x: 20, y: 10, width: 150, height: 150)
       
        groupLabel.frame = CGRect(x: 0, y: 0, width: 150, height: 20)
        groupLabel.textAlignment = NSTextAlignment.Center
       
        cellImageView.frame = CGRect(x: 10, y: 20, width: 130, height: 90)
        
        eventLabel.frame = CGRect(x: 0, y: 115, width: 150, height: 30)
        eventLabel.textAlignment = NSTextAlignment.Center
        eventLabel.font = UIFont(name: eventLabel.font.fontName, size: 25)
        
        itemView.addSubview(groupLabel)
        itemView.addSubview(cellImageView)
        itemView.addSubview(eventLabel)
        
       
        addSubview(itemView)
    
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        
    
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
